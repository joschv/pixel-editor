from PIL import Image


def pixelate(fp, px_len, bit_rounding):
    im = Image.open(fp)
    width = im.size[0]
    height = im.size[1]
    out_im = Image.new("RGB", (width, height), 0)
    px_size = px_len * px_len
    for x in range(0, width, px_len):
        for y in range(0, height, px_len):
            red = green = blue = 0
            for i in range(px_len):
                for j in range(px_len):
                    if x+i < width and y+j < height:
                        pixel = im.getpixel((x + i, y + j))
                        red += pixel[0]
                        green += pixel[1]
                        blue += pixel[2]
            red = int(red / px_size)
            red -= red % bit_rounding
            green = int(green / px_size)
            green -= green % bit_rounding
            blue = int(blue / px_size)
            blue -= blue % bit_rounding
            for i in range(px_len):
                for j in range(px_len):
                    if x+i < width and y+j < height:
                        out_im.putpixel((x + i, y + j), (red, green, blue))
    im.close()
    return out_im


def main():
    default_image = "pics/minnesota.jpg"
    pixelation_rate = 15
    rounding = 32
    out_im = pixelate(default_image, pixelation_rate, rounding)
    out_im.save(default_image.replace(".jpg", "_px" + str(pixelation_rate) + "_r" + str(rounding) + ".jpg"))


if __name__ == '__main__':
    main()
